# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"

GCONF_DEBUG="yes"

inherit gnome2 versionator

if [[ ${PV} = 9999 ]]; then
	inherit gnome2-live
fi

#MATE_BRANCH="$(get_version_component_range 1-2)"

#SRC_URI="http://pub.mate-desktop.org/releases/${MATE_BRANCH}/${P}.tar.xz"
DESCRIPTION="Multimedia related programs for the MATE desktop"
HOMEPAGE="http://mate-desktop.org"

LICENSE="LGPL-2 GPL-2 FDL-1.1"
SLOT="0"
KEYWORDS=""

IUSE="gtk3"

# FIXME: automagic dev-util/glade:3 support
COMMON_DEPEND="app-text/rarian:0
	dev-libs/libxml2:2
	>=dev-libs/glib-2.18.2:2
	gtk3? ( >=x11-libs/gtk+-3.0:3
			media-libs/libcanberra[gtk3]
			dev-libs/libunique:3 )
	!gtk3? ( >=x11-libs/gtk+-2.24:2
			media-libs/libcanberra[gtk]
			dev-libs/libunique:1 )
	>=mate-base/mate-panel-1.8:0[gtk3?]
	>=mate-base/mate-desktop-1.8:0[gtk3?]
	media-libs/libmatemixer
	x11-libs/cairo:0
	x11-libs/pango:0
	virtual/libintl:0"

# Specific gst plugins are used by the default audio encoding profiles
RDEPEND="${COMMON_DEPEND}"

DEPEND="${COMMON_DEPEND}
	app-text/docbook-xml-dtd:4.1.2
	app-text/yelp-tools:0
	>=app-text/scrollkeeper-dtd-1:1.0
	>=dev-util/intltool-0.35.0:*
	sys-devel/gettext:*
	virtual/pkgconfig:*
	!!<mate-base/mate-applets-1.6:*"

src_configure() {
	local myconf

	use gtk3 && myconf="${myconf} --with-gtk=3.0"
	use !gtk3 && myconf="${myconf} --with-gtk=2.0"

	gnome2_src_configure ${myconf}
}

DOCS="AUTHORS ChangeLog* NEWS README"
